INSERT INTO library ("name", "address") VALUES
  ('kosmos', 'Koumakova'),
  ('librarius', 'Kosmakova');

INSERT INTO publishinghouse ("name", "address") VALUES
  ('publishHouse2', 'Newtonova');
  ('publishHouse', 'Ortenova');

INSERT INTO author ("email", "firstname", "surname") VALUES
  ('pavel.novak@seznam.cz','Pavel', 'Novak'),
  ('ondrej.sasek@seznam.cz','Ondrej', 'Sasek');

INSERT INTO book ("isbn", "name", "genre", "publicationdate") VALUES
  ('1234','JajaAPaja', 'comedy', '2010'),
  ('9876','KrtecekAKalhotky', 'tragedy', '2011');
