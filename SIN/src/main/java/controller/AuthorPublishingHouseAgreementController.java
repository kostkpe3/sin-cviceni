package controller;

import entity.Author;
import entity.Book;
import entity.PublishingHouse;
import service.AddBookToLibrary;
import service.AuthorPublishingHouseAgreementImpl;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

//@ManagedBean(name = "agreement")
@Model
@RequestScoped
public class AuthorPublishingHouseAgreementController {

    @Inject
    private AuthorPublishingHouseAgreementImpl authorPublishingHouseAgreement;

    @Inject
    private FacesContext facesContext;

    private String email, name;
    private List<Author> authors;
    private List<PublishingHouse> houses;

    @PostConstruct
    public void init(){
        authors = authorPublishingHouseAgreement.getAuthors();
        houses = authorPublishingHouseAgreement.getHouses();
    }

    public void addAgreement(){
        String note = authorPublishingHouseAgreement.addAgreement(email, name);
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, note, "");
        facesContext.addMessage(null, msg);
        init();
    }

    public Object getEmail() {
        return email;
    }

    public Object getAuthors() {
        return authors;
    }

    public String getName() {
        return name;
    }

    public List<PublishingHouse> getHouses() {
        return houses;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public void setHouses(List<PublishingHouse> houses) {
        this.houses = houses;
    }
}
