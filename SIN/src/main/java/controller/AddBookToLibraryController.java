package controller;

import entity.Book;
import entity.Library;
import service.AddBookToLibraryImpl;
import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.List;

//@ManagedBean
@Model
@RequestScoped
public class AddBookToLibraryController {

    private String isbn, libraryName;
    private List<Library> libraries;
    private List<Book> books;

    @Inject
    private FacesContext facesContext;

    @Inject
    private AddBookToLibraryImpl addBookToLibrary;

    @PostConstruct
    public void init(){
        books = addBookToLibrary.getBooks();
        libraries = addBookToLibrary.getLibraries();
    }

    public void addBookToLibrary(){
        String note = addBookToLibrary.addBookToLibrary(isbn, libraryName);
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, note, "");
        facesContext.addMessage(null, msg);
        init();
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public List<Library> getLibraries() {
        return libraries;
    }

    public void setLibraries(List<Library> libraries) {
        this.libraries = libraries;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
