package controller;

import entity.Book;
import entity.PublishingHouse;
import service.PublishBookImpl;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

//@ManagedBean
@Model
@RequestScoped
public class PublishBookController {

    @Inject
    private PublishBookImpl publishBook;

    @Produces
    @Named
    private Book newBook;

    private String name;
    private List<PublishingHouse> houses;

    @Inject
    private FacesContext facesContext;

    @PostConstruct
    public void init(){
        newBook = new Book();
        houses = publishBook.getHouses();
    }

    public void addNewBook(){
        String note = publishBook.publishNewBook(newBook, name);
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, note, "");
        facesContext.addMessage(null, msg);
        init();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PublishingHouse> getHouses() {
        return houses;
    }

    public void setHouses(List<PublishingHouse> houses) {
        this.houses = houses;
    }
}
