package service;

import entity.Author;
import entity.PublishingHouse;

import java.util.List;

public interface AuthorPublishingHouseAgreement {
    public String addAgreement(String author, String house);

    public List<Author> getAuthors();

    public List<PublishingHouse> getHouses();
}
