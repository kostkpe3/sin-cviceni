package service;

import dao.*;
import entity.Author;
import entity.Book;
import entity.Library;

import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Transactional
public class AddBookToLibraryImpl implements AddBookToLibrary {

    @Inject
    private BookDAOImpl bookDAO;

    @Inject
    private LibraryDAOImpl libraryDAO;

    @Inject
    private Logger logger;

    public String successMessage = "Book successfully added";
    public String notFoundMessage = "Entity not found";
    public String errorMessage = "Error on DAO layer";

    @Override
    public String addBookToLibrary(String isbn, String libraryName) {
            try {
                logger.info("Trying to add book with ISBN " + isbn + " to library " + libraryName);
                Library library = libraryDAO.find(libraryName);
                Book book = bookDAO.find(isbn);
                List<Book> list = library.getBooks();
                list.add(book);
                library.setBooks(list);
                ArrayList<Library> libraries = new ArrayList<>();
                libraries.add(library);
                book.setLibraries(libraries);
                bookDAO.save(book);
                libraryDAO.save(library);
                logger.info("Added book " + book.getName() + " to library " + library.getName());
                return successMessage;
            }catch (NullPointerException e){
                logger.warning(e.toString());
                return notFoundMessage;
            }catch (Exception e){
                logger.warning(e.toString());
                return errorMessage;
            }
    }

    @Override
    public List<Book> getBooks() {
        return bookDAO.list();
    }

    @Override
    public List<Library> getLibraries() {
        return libraryDAO.list();
    }
}
