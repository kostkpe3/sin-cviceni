package service;


import entity.Author;
import entity.Book;
import entity.Library;

import java.util.List;

public interface AddBookToLibrary {
    public String addBookToLibrary(String bookName, String libraryAdress);

    public List<Book> getBooks();

    public List<Library> getLibraries();
}

