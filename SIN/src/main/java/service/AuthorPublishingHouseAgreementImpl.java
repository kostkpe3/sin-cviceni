package service;

import dao.AuthorDAO;
import dao.AuthorDAOImpl;
import dao.PublishingDAOImpl;
import entity.Author;
import entity.PublishingHouse;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Transactional
public class AuthorPublishingHouseAgreementImpl implements AuthorPublishingHouseAgreement {

    @Inject
    private  AuthorDAOImpl authorDAO;

    @Inject
    private PublishingDAOImpl publishingDAO;

    @Inject
    private Logger logger;

    public String successMessage = "Agreement successfully added";
    public String notFoundMessage = "Entity not found";
    public String errorMessage = "Error on DAO layer";

    @Override
    public String addAgreement(String email, String name) {
        try {
            logger.info("Trying to add agreement between " + email + " and " + name);
            Author author = authorDAO.find(email);
            PublishingHouse house = publishingDAO.find(name);
            List<PublishingHouse> houses = author.getPublishingHouses();
            houses.add(house);
            author.setPublishingHouses(houses);
            List<Author> authors = house.getAuthors();
            authors.add(author);
            house.setAuthors(authors);
            authorDAO.save(author);
            publishingDAO.save(house);
            logger.info("Added agreement between " + author.getSurname() + " and " + house.getName());
            return successMessage;
        }catch (NullPointerException e){
            logger.warning(e.toString());
            return notFoundMessage;
        }catch (Exception e){
            logger.warning(e.toString());
            return errorMessage;
        }
    }

    @Override
    public List<Author> getAuthors() {
        return authorDAO.list();
    }

    @Override
    public List<PublishingHouse> getHouses() {
        return publishingDAO.list();
    }

}
