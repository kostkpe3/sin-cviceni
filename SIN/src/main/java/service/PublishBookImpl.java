package service;

import dao.BookDAOImpl;
import dao.PublishingDAOImpl;
import entity.Book;
import entity.Library;
import entity.PublishingHouse;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Transactional
public class PublishBookImpl implements PublishBook {

    @Inject
    private PublishingDAOImpl publishingDAO;

    @Inject
    private BookDAOImpl bookDAO;

    @Inject
    private Logger logger;

    public String successMessage = "Book successfully published";
    public String notFoundMessage = "Entity not found";
    public String errorMessage = "Error on DAO layer";
    public String alreadyExistsMessage = "Book already exists";

    @Override
    public String publishNewBook(Book book, String houseName) {
        try {
            logger.info(houseName + " trying to publish book " + book.getName());

            if (bookDAO.find(book.getISBN()) != null) {
                logger.info("Book with ISBN " + book.getISBN() + " already exists.");
                return alreadyExistsMessage;
            }
            PublishingHouse house = publishingDAO.find(houseName);
            book.setPublishingHouse(house);
            bookDAO.save(book);

            logger.info("Book " + book.getName() + " published.");
            return successMessage;

        }catch (NullPointerException e){
            logger.warning(e.toString());
            return notFoundMessage;

        }catch (Exception e){
            logger.warning(e.toString());
            return errorMessage;
        }
    }

    @Override
    public List<PublishingHouse> getHouses() {
        return publishingDAO.list();
    }
}
