package service;

import entity.Book;
import entity.PublishingHouse;

import java.util.List;

public interface PublishBook {
    public String publishNewBook(Book book, String houseName);

    public List<PublishingHouse> getHouses();
}
