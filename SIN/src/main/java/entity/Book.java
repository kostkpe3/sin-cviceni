package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "book")
public class Book implements Serializable {

    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "genre")
    private String Genre;

    @Id
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "isbn")
    private String ISBN;

    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "name")
    private String Name;

    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "publicationdate")
    private String PublicationDate;

    //@NotNull
    @OneToOne(cascade = CascadeType.ALL)
    private PublishingHouse publishingHouse;

    @ManyToMany
    @JoinTable(name = "author_book",
            joinColumns = @JoinColumn(name = "isbn"),
            inverseJoinColumns = @JoinColumn(name = "email")
    )
    private List<Author> authors = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "library_book",
            joinColumns = @JoinColumn(name = "isbn"),
            inverseJoinColumns = @JoinColumn(name = "name")
    )
    private List<Library> libraries = new ArrayList<>();

    public PublishingHouse getPublishingHouse() {
        return publishingHouse;
    }

    public List<Library> getLibraries() {
        return libraries;
    }

    public void setLibraries(List<Library> libraries) {
        this.libraries = libraries;
    }

    public void setPublishingHouse(PublishingHouse publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String genre) {
        Genre = genre;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPublicationDate() {
        return PublicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        PublicationDate = publicationDate;
    }
}
