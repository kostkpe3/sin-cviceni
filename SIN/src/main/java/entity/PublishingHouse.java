package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "publishinghouse")
public class PublishingHouse implements Serializable {

    @Id
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "name")
    private String name;

    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "address")
    private String address;

    @ManyToMany(mappedBy = "publishingHouses")
    private List<Author> authors = new ArrayList<>();

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String adress) {
        this.address = adress;
    }
}
