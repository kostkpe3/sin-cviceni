package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "library")
public class Library implements Serializable {
    @Id
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "name")
    private String name;

    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "address")
    private String address;

    @ManyToMany(mappedBy = "libraries")
    private List<Book> books = new ArrayList<>();


    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String adress) {
        this.address = adress;
    }
}
