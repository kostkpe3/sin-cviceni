package dao;

import entity.PublishingHouse;

public interface PublishingHouseDAO extends DAO<PublishingHouse> {
}
