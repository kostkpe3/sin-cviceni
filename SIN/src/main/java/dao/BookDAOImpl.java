package dao;

import entity.Book;

import javax.inject.Named;

@Named
public class BookDAOImpl extends GenericDAO<Book> implements BookDAO {
}
