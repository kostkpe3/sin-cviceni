package dao;

import entity.Library;

public interface LibraryDAO extends DAO<Library> {
}
