package dao;

import entity.Book;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.*;
import org.junit.runner.RunWith;
import util.Resource;

import javax.inject.Inject;

@RunWith(Arquillian.class)
public class TestBookDAO {

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(Book.class.getPackage())
                .addPackage(BookDAOImpl.class.getPackage())
                .addClass(Resource.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    private BookDAOImpl bookDAO;

    @Test
    @Transactional
    public void createBookTest() {
        String ISBN = "123456789";
        Book newBook = new Book();
        newBook.setISBN(ISBN);
        newBook.setGenre("comedy");
        newBook.setName("JajaAPaja");
        newBook.setPublicationDate("2010");
        bookDAO.save(newBook);
        Assert.assertEquals(bookDAO.find(ISBN).getISBN(), ISBN);
    }


}
