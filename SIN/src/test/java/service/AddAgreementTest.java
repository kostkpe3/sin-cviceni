package service;

import dao.AuthorDAOImpl;
import dao.BookDAOImpl;
import dao.LibraryDAOImpl;
import dao.PublishingDAOImpl;
import entity.Author;
import entity.Book;
import entity.Library;
import entity.PublishingHouse;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.Resource;

import javax.inject.Inject;

@RunWith(Arquillian.class)
public class AddAgreementTest {

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test2.war")
                .addPackage(Author.class.getPackage())
                .addPackage(AuthorDAOImpl.class.getPackage())
                .addPackage(AuthorPublishingHouseAgreementImpl.class.getPackage())
                .addPackage(PublishingDAOImpl.class.getPackage())
                .addPackage(PublishingHouse.class.getPackage())
                .addClass(Resource.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    private AuthorPublishingHouseAgreementImpl authorPublishingHouseAgreement;

    @Inject
    private AuthorDAOImpl authorDAO;

    @Inject
    private PublishingDAOImpl publishingDAO;

    @Test
    @Transactional
    public void testAdd() {
        String email = "pan.petr@zemenezeme.lol";
        Author author= new Author();
        author.setFirstName("pan");
        author.setSurname("petr");
        author.setEmail(email);
        authorDAO.save(author);

        String name = "house";
        PublishingHouse house = new PublishingHouse();
        house.setName(name);
        house.setAddress("Somewhere");
        publishingDAO.save(house);
        Assert.assertEquals(authorPublishingHouseAgreement.addAgreement(email, name), authorPublishingHouseAgreement.successMessage);
    }
}

