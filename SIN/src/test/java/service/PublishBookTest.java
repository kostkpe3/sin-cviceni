package service;

import dao.AuthorDAOImpl;
import dao.BookDAOImpl;
import dao.LibraryDAOImpl;
import dao.PublishingDAOImpl;
import entity.Author;
import entity.Book;
import entity.Library;
import entity.PublishingHouse;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.junit.Test;
import util.Resource;

import javax.ejb.EJBException;
import javax.inject.Inject;

@RunWith(Arquillian.class)
public class PublishBookTest {

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test2.war")
                .addPackage(Book.class.getPackage())
                .addPackage(BookDAOImpl.class.getPackage())
                .addPackage(PublishBookImpl.class.getPackage())
                .addPackage(PublishingDAOImpl.class.getPackage())
                .addPackage(PublishingHouse.class.getPackage())
                .addClass(Resource.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    private PublishBookImpl publishBook;

    @Inject
    private PublishingDAOImpl publishingDAO;

    @Inject
    private BookDAOImpl bookDAO;

    @Test
    @Transactional
    public void testPublish() {
        String name = "Albatros";
        PublishingHouse house = new PublishingHouse();
        house.setName(name);
        house.setAddress("Ortenova");
        publishingDAO.save(house);

        String isbn = "12345";
        Book book = new Book();
        book.setISBN(isbn);
        book.setName("JajaAPaja");
        book.setGenre("comedy");
        book.setPublicationDate("2010");
        Assert.assertEquals(publishBook.publishNewBook(book, name), publishBook.successMessage);
        Assert.assertEquals(bookDAO.find(isbn).getISBN(), isbn);
    }

    @Test
    @Transactional
    public void testPublishSameBook() {
        String isbn = "123456";
        Book book = new Book();
        book.setISBN(isbn);
        book.setName("JajaAPaja");
        book.setGenre("comedy");
        book.setPublicationDate("2010");
        bookDAO.save(book);

        String name = "Nsstros";
        PublishingHouse house = new PublishingHouse();
        house.setName(name);
        house.setAddress("Koumakova");
        publishingDAO.save(house);

        Book book2 = new Book();
        book2.setISBN(isbn);
        book2.setName("JajaAPaja");
        book2.setGenre("comedy");
        book2.setPublicationDate("2010");
        Assert.assertEquals(publishBook.publishNewBook(book2, name), publishBook.alreadyExistsMessage);
    }
}
