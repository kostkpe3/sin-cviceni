package service;

import dao.AuthorDAOImpl;
import dao.BookDAOImpl;
import dao.LibraryDAOImpl;
import entity.Author;
import entity.Book;
import entity.Library;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.junit.Test;
import util.Resource;

import javax.ejb.EJBException;
import javax.inject.Inject;

@RunWith(Arquillian.class)
public class AddBookToLibraryTest {

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test2.war")
                .addPackage(Book.class.getPackage())
                .addPackage(BookDAOImpl.class.getPackage())
                .addPackage(AddBookToLibraryImpl.class.getPackage())
                .addPackage(LibraryDAOImpl.class.getPackage())
                .addPackage(Library.class.getPackage())
                .addClass(Resource.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    private AddBookToLibraryImpl addBookToLibrary;

    @Inject
    private LibraryDAOImpl libraryDAO;

    @Inject
    private BookDAOImpl bookDAO;

    @Test
    @Transactional
    public void testAdd() {
        String name = "Librarius";
        Library library = new Library();
        library.setAddress("Kosmakova");
        library.setName(name);
        libraryDAO.save(library);

        String isbn = "12345";
        Book book = new Book();
        book.setISBN(isbn);
        book.setName("JajaAPaja");
        book.setGenre("comedy");
        book.setPublicationDate("2010");
        bookDAO.save(book);
        Assert.assertEquals(addBookToLibrary.addBookToLibrary(isbn, name), addBookToLibrary.successMessage);
    }

    @Test
    @Transactional
    public void testAddWithWrongAddress() {
        String isbn = "123456";
        Book book = new Book();
        book.setISBN(isbn);
        book.setName("JajaAPaja");
        book.setGenre("comedy");
        book.setPublicationDate("2010");
        bookDAO.save(book);
        Assert.assertEquals(addBookToLibrary.addBookToLibrary(isbn, "nic"), addBookToLibrary.notFoundMessage);
    }
}
